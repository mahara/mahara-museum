<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2007 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Alastair Pharo <alastair@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Blogs';

$string['blog'] = 'Blog';
$string['blogs'] = 'Blogs';
$string['addblog'] = 'Add Blog';
$string['addpost'] = 'Add Post';
$string['alignment'] = 'Alignment';
$string['attach'] = 'Attach';
$string['attachedfilelistloaded'] = 'Attached file list loaded';
$string['attachedfiles'] = 'Attached files';
$string['attachments'] = 'Attachments';
$string['blogdesc'] = 'Description';
$string['blogdescdesc'] = 'e.g., ‘A record of Jill\'s experiences and reflections’.';
$string['blogdoesnotexist'] = 'You are trying to access a blog that does not exist';
$string['blogpostdoesnotexist'] = 'You are trying to access a blog post that does not exist';
$string['blogfilesdirdescription'] = 'Files uploaded as blog post attachments';
$string['blogfilesdirname'] = 'blogfiles';
$string['blogpost'] = 'Blog Post';
$string['blogdeleted'] = 'Blog deleted';
$string['blogpostdeleted'] = 'Blog post deleted';
$string['blogpostpublished'] = 'Blog post published';
$string['blogpostsaved'] = 'Blog post saved';
$string['blogsettings'] = 'Blog Settings';
$string['blogtitle'] = 'Title';
$string['blogtitledesc'] = 'e.g., ‘Jill’s Nursing Practicum Journal’.';
$string['border'] = 'Border';
$string['browsemyfiles'] = 'Browse my files';
$string['cancel'] = 'Cancel';
$string['commentsallowed'] = 'Comments';
$string['commentsalloweddesc'] = ' ';
$string['commentsallowedno'] = 'Do not allow comments on this blog';
$string['commentsallowedyes'] = 'Allow logged in users to comment on this blog';
$string['commentsnotify'] = 'Comment Notification';
$string['commentsnotifydesc'] = 'You can optionally receive notification whenever someone adds a comment to one of your blog posts.';
$string['commentsnotifyno'] = 'Do not notify me of comments to this blog';
$string['commentsnotifyyes'] = 'Notify me of comments to this blog';
$string['createandpublishdesc'] = 'This will create the blog post and make it available to others.';
$string['createasdraftdesc'] = 'This will create the blog post, but it will not become available to others until you choose to publish it.';
$string['createblog'] = 'Create Blog';
$string['delete'] = 'Delete';
$string['deleteblog?'] = 'Are you sure you want to delete this blog?';
$string['deleteblogpost?'] = 'Are you sure you want to delete this post?';
$string['description'] = 'Description';
$string['dimensions'] = 'Dimensions';
$string['draft'] = 'Draft';
$string['edit'] = 'Edit';
$string['editblogpost'] = 'Edit Blog Post';
$string['erroraccessingblogfilesfolder'] = 'Error accessing blogfiles folder';
$string['errorsavingattachments'] = 'An error occurred while saving blog post attachments';
$string['horizontalspace'] = 'Horizontal space';
$string['insert'] = 'Insert';
$string['insertimage'] = 'Insert image';
$string['mustspecifytitle'] = 'You must specify a title for your post';
$string['mustspecifycontent'] = 'You must specify some content for your post';
$string['myblogs'] = 'My Blogs';
$string['name'] = 'Name';
$string['newattachmentsexceedquota'] = 'The total size of the new files that you have uploaded to this post would exceed your quota.  You may be able to save the post if you remove some of the attachments you have just added.';
$string['newblog'] = 'New Blog';
$string['newblogpost'] = 'New Blog Post';
$string['noimageshavebeenattachedtothispost'] = 'No images have been attached to this post.  You need to upload or attach an image to the post before you can insert it.';
$string['nofilesattachedtothispost'] = 'No attached files';
$string['noresults'] = 'No blog posts found';
$string['postbody'] = 'Body';
$string['postbodydesc'] = ' ';
$string['postedon'] = 'Posted on';
$string['postedbyon'] = 'Posted by %s on %s';
$string['posttitle'] = 'Title';
$string['posttitledesc'] = 'The title appears above your post.';
$string['posts'] = 'posts';
$string['publish'] = 'Publish';
$string['publishfailed'] = 'An error occurred.  Your post was not published';
$string['publishblogpost?'] = 'Are you sure you want to publish this post?';
$string['published'] = 'Published';
$string['remove'] = 'Remove';
$string['save'] = 'Save';
$string['saveandpublish'] = 'Save and Publish';
$string['saveasdraft'] = 'Save as Draft';
$string['savepost'] = 'Save post';
$string['savesettings'] = 'Save Settings';
$string['settings'] = 'Settings';
$string['thisisdraft'] = 'This post is a draft';
$string['thisisdraftdesc'] = 'When your post is a draft, no one except you can see it.';
$string['title'] = 'Title';
$string['update'] = 'Update';
$string['verticalspace'] = 'Vertical space';
$string['viewblog'] = 'View Blog';
$string['youarenottheownerofthisblog'] = 'You are not the owner of this blog';
$string['youarenottheownerofthisblogpost'] = 'You are not the owner of this blog post';
$string['cannotdeleteblogpost'] = 'An error occured removing this blog post.';

$string['baseline'] = 'Baseline';
$string['top'] = 'Top';
$string['middle'] = 'Middle';
$string['bottom'] = 'Bottom';
$string['texttop'] = 'Text top';
$string['absolutemiddle'] = 'Absolute middle';
$string['absolutebottom'] = 'Absolute bottom';
$string['left'] = 'Left';
$string['right'] = 'Right';

?>

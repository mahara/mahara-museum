<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2007 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage notification-internal
 * @author     Penny Leach <penny@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['typemaharamessage'] = 'System message';
$string['typeusermessage'] = 'Message from other users';
$string['typefeedback'] = 'Feedback';
$string['typewatchlist'] = 'Watchlist';
$string['typeviewaccess'] = 'New view access';
$string['typecontactus'] = 'Contact us';
$string['typeobjectionable'] = 'Objectionable content';
$string['typevirusrepeat'] = 'Repeat virus upload';
$string['typevirusrelease'] = 'Virus flag release';
$string['typeadminmessages'] = 'Administration messages';

$string['type'] = 'Activity type';
$string['attime'] = 'at';
$string['prefsdescr'] = 'If you select either of the email options, notifications will still be entered in the Activity log, but they will be automatically marked as read.';

$string['subject'] = 'Subject';
$string['date'] = 'Date';
$string['read'] = 'Read';
$string['unread'] = 'Unread';

$string['markasread'] = 'Mark as read';
$string['selectall'] = 'Select all';
$string['recurseall'] = 'Recurse all';
$string['alltypes'] = 'All types';

$string['markedasread'] = 'Marked your notifications as read';
$string['failedtomarkasread'] = 'Failed to mark your notifications as read';

$string['stopmonitoring'] = 'Stop monitoring';
$string['viewsandartefacts'] = 'Views and Artefacts';
$string['views'] = 'Views';
$string['artefacts'] = 'Artefacts';
$string['groups'] = 'Groups';
$string['monitored'] = 'Monitored';
$string['stopmonitoring'] = 'Stop monitoring';

$string['stopmonitoringsuccess'] = 'Stopped monitoring successfully';
$string['stopmonitoringfailed'] = 'Failed to stop monitoring';

$string['newwatchlistmessagesubject'] = 'New activity on your watchlist';
$string['newwatchlistmessageview'] = 'has changed their view "%s"';

$string['newcontactusfrom'] = 'New contact us from';
$string['newcontactus'] = 'New contact us';
$string['newfeedbackonview'] = 'New feedback on view';
$string['newfeedbackonartefact'] = 'New feedback on artefact';

$string['newviewaccessmessage'] = 'You have been added to the access list for the view called';
$string['newviewaccesssubject'] = 'New view access';

$string['viewmodified'] = 'has changed their view';
$string['onview'] = 'on View';
$string['onartefact'] = 'on Artefact';
$string['ongroup'] = 'on Group';
$string['ownedby'] = 'owned by';

$string['objectionablecontentview'] = 'Objectionable content';
$string['objectionablecontentartefact'] = 'Objectionable content';

$string['newgroupmembersubj'] = '%s is now a group member!';
$string['removedgroupmembersubj'] = '%s is no longer a group member';

$string['addtowatchlist'] = 'Add to watchlist';
$string['removefromwatchlist'] = 'Remove from watchlist';

?>

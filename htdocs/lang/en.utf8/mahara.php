<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2007 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['add']     = 'Add';
$string['cancel']  = 'Cancel';
$string['delete']  = 'Delete';
$string['edit']    = 'Edit';
$string['editing'] = 'Editing';
$string['save']    = 'Save';
$string['submit']  = 'Submit';
$string['update']  = 'Update';
$string['change']  = 'Change';
$string['send']    = 'Send';
$string['go']      = 'Go';
$string['default'] = 'Default';
$string['upload']  = 'Upload';
$string['complete']  = 'Complete';
$string['Failed']  = 'Failed';
$string['loading'] = 'Loading ...';
$string['showtags'] = 'Show my tags';
$string['errorprocessingform'] = 'There was an error with submitting this form. Please check the marked fields and try again.';

$string['no']     = 'No';
$string['yes']    = 'Yes';
$string['none']   = 'None';
$string['at'] = 'at';
$string['From'] = 'From';
$string['To'] = 'To';

$string['next']      = 'Next';
$string['nextpage']  = 'Next page';
$string['previous']  = 'Previous';
$string['prevpage']  = 'Previous page';
$string['first']     = 'First';
$string['firstpage'] = 'First page';
$string['last']      = 'Last';
$string['lastpage']  = 'Last page';

$string['accept'] = 'Accept';
$string['reject'] = 'Reject';
$string['sendrequest'] = 'Send request';
$string['reason'] = 'Reason';
$string['select'] = 'Select';

$string['tags'] = 'Tags';
$string['tagsdesc'] = 'Enter comma separated tags for this item';

$string['selfsearch'] = 'Search My Portfolio';
$string['ssopeers'] = 'SSO Peers';

// Quota strings
$string['quota'] = 'Quota';
$string['quotausage'] = 'You have used <span id="quota_used">%s</span> of your <span id="quota_total">%s</span> quota.';

$string['mainmenu'] = 'Main menu';
$string['updatefailed'] = 'Update failed';
$string['declinerequest'] = 'Decline request';

$string['strftimenotspecified']  = 'Not specified';

// auth
$string['accessforbiddentoadminsection'] = 'You are forbidden from accessing the administration section';
$string['accountdeleted'] = 'Sorry, your account has been deletd';
$string['accountexpired'] = 'Sorry, your account has expired';
$string['accountexpirywarning'] = 'Account expiry warning';
$string['accountexpirywarningtext'] = 'Dear %s,

Your account on %s will expire within %s.

We recommend you save the contents of your portfolio using the Export tool. Instructions on using this feature may be found within the user guide.

If you wish to extend your account access or have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['accountexpirywarninghtml'] = '<p>Dear %s,</p>
    
<p>Your account on %s will expire within %s.</p>

<p>We recommend you save the contents of your portfolio using the Export tool. Instructions on using this feature may be found within the user guide.</p>

<p>If you wish to extend your account access or have any questions regarding the above, please feel free to <a href="%s">Contact Us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['accountinactive'] = 'Sorry, your account is currently inactive';
$string['accountinactivewarning'] = 'Account inactivity warning';
$string['accountinactivewarningtext'] = 'Dear %s,

Your account on %s will become inactive within %s.

Once inactive, you will not be able to log in until an administrator re-enables your account.

You can prevent your account from becoming inactive by logging in.

Regards, %s Site Administrator';
$string['accountinactivewarninghtml'] = '<p>Dear %s,</p>

<p>Your account on %s will become inactive within %s.</p>

<p>Once inactive, you will not be able to log in until an administrator re-enables your account.</p>

<p>You can prevent your account from becoming inactive by logging in.</p>

<p>Regards, %s Site Administrator</p>';
$string['accountsuspended'] = 'Your account has been suspeneded as of %s. The reason for your suspension is:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'Your account has been suspeneded';
$string['youraccounthasbeenunsuspended'] = 'Your account has been unsuspeneded';
$string['changepassword'] = 'Change Password';
$string['changepasswordinfo'] = 'You are required to change your password before you can proceed.';
$string['confirmpassword'] = 'Confirm password';
$string['javascriptnotenabled'] = 'Your browser does not have javascript enabled for this site. Mahara requires javascript to be enabled before you can log in';
$string['cookiesnotenabled'] = 'Your browser does not have cookies enabled, or is blocking cookies from this site. Mahara requires cookies to be enabled before you can log in';
$string['institution'] = 'Institution';
$string['loggedoutok'] = 'You have been logged out successfully';
$string['login'] = 'Login';
$string['loginfailed'] = 'You have not provided the correct credentials to log in. Please check your username and password are correct.';
$string['loginto'] = 'Log in to %s';
$string['newpassword'] = 'New Password';
$string['nosessionreload'] = 'Reload the page to log in';
$string['oldpassword'] = 'Current Password';
$string['password'] = 'Password';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = 'The password you use to access the system';
$string['passwordnotchanged'] = 'You did not change your password, please choose a new password';
$string['passwordsaved'] = 'Your new password has been saved';
$string['passwordsdonotmatch'] = 'The passwords do not match';
$string['passwordtooeasy'] = 'Your password is too easy! Please choose a harder password';
$string['register'] = 'Register';
$string['sessiontimedout'] = 'Your session has timed out, please enter your login details to continue';
$string['sessiontimedoutpublic'] = 'Your session has timed out. You may <a href="?login">log in</a> to continue browsing';
$string['sessiontimedoutreload'] = 'Your session has timed out. Reload the page to log in again';
$string['username'] = 'Username';
$string['preferredname'] = 'Preferred Name';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'The username you have been given to access this system.';
$string['yournewpassword'] = 'Your new password';
$string['yournewpasswordagain'] = 'Your new password again';
$string['invalidsesskey'] = 'Invalid session key';
$string['cannotremovedefaultemail'] = 'You cannot remove your primary email address';
$string['emailtoolong'] = 'E-mail addresses cannot be longer that 255 characters';
$string['mustspecifyoldpassword'] = 'You must specify your current password';
$string['captchatitle'] = 'CAPTCHA Image';
$string['captchaimage'] = 'CAPTCHA Image';
$string['captchadescription'] = 'Enter the characters you see in the picture to the right. Letters are not case sensitive';
$string['captchaincorrect'] = 'Enter the letters as they are shown in the image';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = 'Email address';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'First name';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'Last name';
$string['lastnamedescription'] = ' ';
$string['registerstep1description'] = 'Welcome! To use this site you must first register. You must also agree to the <a href="terms.php">terms and conditions</a>. The data we collect here will be stored according to our <a href="privacy.php">privacy statement</a>.';
$string['registerstep3fieldsoptional'] = '<h3>Choose an Optional Profile Image</h3><p>You have now successfully registered with ' . get_config('sitename') . '! You may now choose an optional profile icon to be displayed as your avatar.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>Fill Out Mandatory Profile Fields</h3><p>The following fields are required. You must fill them out before your registration is complete.</p>';
$string['registeringdisallowed'] = 'Sorry, you cannot register for this system at this time';

// Forgot password
$string['cantchangepassword'] = 'Sorry, you are unable to change your password through this interface - please use your institution\'s interface instead';
$string['forgotpassword'] = 'Forgotten your password?';
$string['forgotpasswordtext'] = 'If you have forgotten your password, enter below the primary email address you have listed in your Profile and we will send you a key you can use to give yourself a new password';
$string['passwordreminder'] = 'Password Reminder';
$string['pwchangerequestsent'] = 'You should receive an e-mail shortly with a link you can use to change the password for your account';
$string['forgotpassemailsubject'] = 'Change password request for %s';
$string['forgotpassemailmessagetext'] = 'Dear %s,

A request to reset your password has been received for your %s account.

Please follow the link below to continue the reset process.

' . get_config('wwwroot') . 'forgotpass.php?key=%s

If you did not request a password reset, please ignore this email.

If you have any questsions regarding the above, please feel free to contact
us.

' . get_config('wwwroot') . 'contact.php

Regards, %s Site Administrator

' . get_config('wwwroot') . 'forgotpass.php?key=%s';
$string['forgotpassemailmessagehtml'] = '<p>Dear %s,</p>

<p>A request to reset your password has been received for your %s account.</p>

<p>Please follow the link below to continue the reset process.</p>

<p><a href="' . get_config('wwwroot') . 'forgotpass.php?key=%s">' . get_config('wwwroot') . 'forgotpass.php?key=%s</a></p>

<p>If you did not request a password reset, please ignore this email.</p>

<p>If you have any questsions regarding the above, please feel free to <a href="' . get_config('wwwroot') . 'contact.php">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>

<p><a href="' . get_config('wwwroot') . 'forgotpass.php?key=%s">' . get_config('wwwroot') . 'forgotpass.php?key=%s</a></p>';
$string['forgotpassemailsendunsuccessful'] = 'Sorry, it appears that the e-mail could not be sent successfully. This is our fault, please try again shortly';
$string['forgotpassnosuchemailaddress'] = 'The email address you entered doesn\'t match any users for this site';
$string['forgotpasswordenternew'] = 'Please enter your new password to continue';
$string['nosuchpasswordrequest'] = 'No such password request';
$string['passwordchangedok'] = 'Your password was successfully changed';

// Expiry times
$string['noenddate'] = 'No end date';
$string['day']       = 'day';
$string['days']      = 'days';
$string['weeks']     = 'weeks';
$string['months']    = 'months';
$string['years']     = 'years';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = '%s text not available';

// Contact us form
$string['name']                     = 'Name';
$string['email']                    = 'Email';
$string['subject']                  = 'Subject';
$string['message']                  = 'Message';
$string['messagesent']              = 'Your message has been sent';
$string['nosendernamefound']        = 'No sender name was submitted';
$string['emailnotsent']             = 'Failed to send contact email. Error message: "%s"';

// mahara.js
$string['namedfieldempty'] = 'The required field "%s" is empty';
$string['processing']     = 'Processing';
$string['requiredfieldempty'] = 'A required field is empty';
$string['unknownerror']       = 'An unknown error occurred (0x20f91a0)';

// menu
$string['home']        = 'Home';
$string['myportfolio'] = 'My Portfolio';
$string['settings']    = 'Settings';
$string['groupsimin']  = 'Groups I\'m In';
$string['groupsiown']  = 'Groups I Own';
$string['myfriends']          = 'My Friends';
$string['youareloggedinas']   = 'You are logged in as %s';
$string['unreadmessages'] = 'unread messages';
$string['unreadmessage'] = 'unread message';

// footer
$string['termsandconditions'] = 'Terms and Conditions';
$string['privacystatement']   = 'Privacy Statement';
$string['about']              = 'About';
$string['contactus']          = 'Contact Us';

// my account
$string['account'] =  'My account';
$string['accountprefs'] = 'Preferences';
$string['preferences'] = 'Preferences';
$string['activityprefs'] = 'Activity preferences';
$string['changepassword'] = 'Change password';
$string['notifications'] = 'Notifications';

// my views
$string['accessstartdate'] = 'Access start date';
$string['accessstopdate'] = 'Access end date';
$string['artefacts'] = 'Artefacts';
$string['createnewview'] = 'Create New View';
$string['deleteviewfailed'] = 'Delete view failed';
$string['deleteviewquestion'] = 'Do you really want to delete this view?';
$string['description'] = 'Description';
$string['editaccess'] = 'Edit Access';
$string['editview'] = 'Edit View';
$string['editviewinformation'] = 'Edit View Information';
$string['myviews'] = 'My Views';
$string['notownerofview'] = 'You are not the owner of this view';
$string['reallyaddaccesstoemptyview'] = 'Your view contains no artefacts.  Do you really want to give these users access to the view?';
$string['saveaccess'] = 'Save Access';
$string['submitview'] = 'Submit View';
$string['submitviewfailed'] = 'Submit view failed';
$string['submitviewquestion'] = 'If you submit this view for assessment, you will not be able to edit the view or any of its associated artefacts until your tutor has finished marking the view.  Are you sure you want to submit this view now?';
$string['viewdeleted'] = 'View deleted';
$string['views'] = 'Views';
$string['viewsubmitted'] = 'View submitted';
$string['viewsubmittedto'] = 'This view has been submitted to %s';

// access levels
$string['public'] = 'Public';
$string['loggedin'] = 'Logged In Users';
$string['friends'] = 'Friends';
$string['groups'] = 'Groups';
$string['users'] = 'Users';

// view user
$string['fullname'] = 'Full name';
$string['displayname'] = 'Display name';
$string['studentid'] = 'ID number';
$string['inviteusertojoingroup'] = 'Invite this user to join a group';
$string['addusertogroup'] = 'Add this user to a group';

$string['emailname'] = 'Mahara System'; // robot! 

$string['config'] = 'Config';

$string['notinstallable'] = 'Not installable!';
$string['installedplugins'] = 'Installed plugins';
$string['notinstalledplugins'] = 'Not installed plugins';
$string['plugintype'] = 'Plugin type';

$string['settingssaved'] = 'Settings saved';
$string['settingssavefailed'] = 'Failed to save settings';

$string['width'] = 'Width';
$string['height'] = 'Height';
$string['widthshort'] = 'w';
$string['heightshort'] = 'h';
$string['filter'] = 'Filter';
$string['expand'] = 'Expand';
$string['collapse'] = 'Collapse';
$string['more...'] = 'More ...';
$string['nohelpfound'] = 'There was no help found for this item';
$string['nohelpfoundpage'] = 'There was no help found for this page';
$string['couldnotgethelp'] = 'An error occurred trying to retrive the help page';
$string['profileimage'] = 'Profile image';
$string['primaryemailinvalid'] = 'Your primary email address is invalid';
$string['addemail'] = 'Add email address';

// Search
$string['search'] = 'Search';
$string['Query'] = 'Query';
$string['advancedsearch'] = 'Advanced search';
$string['query'] = 'Query';
$string['querydescription'] = 'The words to be searched for';
$string['result'] = 'result';
$string['results'] = 'results';
$string['Results'] = 'Results';
$string['noresultsfound'] = 'No results found';

// artefact
$string['artefact'] = 'artefact';
$string['Artefact'] = 'Artefact';
$string['artefactnotfound'] = 'Artefact with id %s not found';
$string['artefactnotrendered'] = 'Artefact not rendered';

$string['belongingto'] = 'Belonging to';
$string['allusers'] = 'All users';

// view view
$string['addedtowatchlist'] = 'This view has been added to your watchlist';
$string['removedfromwatchlist'] = 'This view has been removed from your watchlist';
$string['addfeedbackfailed'] = 'Add feedback failed';
$string['addtowatchlist'] = 'Add view to watchlist';
$string['removefromwatchlist'] = 'Remove view from watchlist';
$string['alreadyinwatchlist'] = 'This view is already in your watchlist';
$string['attachedfileaddedtofolder'] = "The attached file %s has been added to your '%s' folder.";
$string['attachfile'] = "Attach file";
$string['complaint'] = 'Complaint';
$string['date'] = 'Date';
$string['feedback'] = 'Feedback';
$string['feedbackattachdirname'] = 'assessmentfiles';
$string['feedbackattachdirdesc'] = 'Files attached to view assessments';
$string['feedbackattachmessage'] = 'The attached file has been added to your %s folder';
$string['feedbackmadeprivate'] = 'Feedback changed to private';
$string['feedbackonthisartefactwillbeprivate'] = 'Feedback on this artefact will only be visible to the owner.';
$string['feedbackonviewbytutorofgroup'] = 'Feedback on %s by %s of %s';
$string['feedbacksubmitted'] = 'Feedback submitted';
$string['makepublic'] = 'Make public';
$string['nopublicfeedback'] = 'No public feedback';
$string['notifysiteadministrator'] = 'Notify site administrator';
$string['placefeedback'] = 'Place feedback';
$string['print'] = 'Print';
$string['private'] = 'Private';
$string['makeprivate'] = 'Change to Private';
$string['reportobjectionablematerial'] = 'Report objectionable material';
$string['reportsent'] = 'Your report has been sent';
$string['updatewatchlistfailed'] = 'Update of watchlist failed';
$string['view'] = 'view';
$string['View'] = 'View';
$string['watchlistupdated'] = 'Your watchlist has been updated';

// group
$string['groupname'] = 'Group Name';
$string['creategroup'] = 'Create Group';
$string['groupmemberrequests'] = 'Pending membership requests';
$string['addgroup'] = 'Add new group';
$string['sendinvitation'] = 'Send invite';
$string['invitetogroupsubject'] = 'You were invited to join a group';
$string['invitetogroupmessage'] = '%s has invited you to join a group, \'%s\'.  Click on the link below for more information.';
$string['inviteuserfailed'] = 'Failed to invite the user';
$string['userinvited'] = 'Invite sent';
$string['addedtogroupsubject'] = 'You were added to a group';
$string['addedtogroupmessage'] = '%s has added you to a group, \'%s\'.  Click on the link below to see the group';
$string['adduserfailed'] = 'Failed to add the user';
$string['useradded'] = 'User added';
$string['editgroup'] = 'Edit Group';
$string['savegroup'] = 'Save Group';
$string['groupsaved'] = 'Group Saved Successfully';
$string['groupname'] = 'Group Name';
$string['invalidgroup'] = 'The group doesn\'t exist';
$string['canteditdontown'] = 'You can\'t edit this group because you don\'t own it';
$string['groupdescription'] = 'Group Description';
$string['membershiptype'] = 'Group Membership Type';
$string['membershiptype.controlled'] = 'Controlled Membership';
$string['membershiptype.invite']     = 'Invite Only';
$string['membershiptype.request']    = 'Request Membership';
$string['membershiptype.open']       = 'Open Membership';
$string['pendingmembers']            = 'Pending Members';
$string['reason']                    = 'Reason';
$string['approve']                   = 'Approve';
$string['reject']                    = 'Reject';
$string['groupalreadyexists'] = 'A Group by this name already exists';
$string['owner'] = 'Owner';
$string['members'] = 'Members';
$string['memberrequests'] = 'Membership requests';
$string['submittedviews'] = 'Submitted views';
$string['releaseview'] = 'Release view';
$string['tutor'] = 'Tutor';
$string['tutors'] = 'Tutors';
$string['member'] = 'Member';
$string['invite'] = 'Invite';
$string['remove'] = 'Remove';
$string['updatemembership'] = 'Update membership';
$string['memberchangefailed'] = 'Failed to update some membership information';
$string['memberchangesuccess'] = 'Membership status changed successfully';
$string['viewreleasedsubject'] = 'Your view has been released';
$string['viewreleasedmessage'] = 'The view that you submitted to group %s has been released back to you by %s';
$string['viewreleasedsuccess'] = 'View was released successfully';
$string['groupmembershipchangesubject'] = 'Group membership: %s';
$string['groupmembershipchangemessagetutor'] = 'You have been promoted to a tutor in this group';
$string['groupmembershipchangemessagemember'] = 'You have been demoted from a tutor in this group';
$string['groupmembershipchangemessageremove'] = 'You have been removed from this group';
$string['groupmembershipchangemessagedeclinerequest'] = 'Your request to join this group has been declined';
$string['groupmembershipchangedmessageaddedtutor'] = 'You have been added as a tutor in this group';
$string['groupmembershipchangedmessageaddedmember'] = 'You have been added as a member in this group';
$string['leavegroup'] = 'Leave this group';
$string['joingroup'] = 'Join this group';
$string['requestjoingroup'] = 'Request to join this group';
$string['grouphaveinvite'] = 'You have been invited to join this group';
$string['groupnotinvited'] = 'You have not been invited to join this group';
$string['groupinviteaccepted'] = 'Invite accepted successfully! You are now a group member';
$string['groupinvitedeclined'] = 'Invite declined successfully!';
$string['acceptinvitegroup'] = 'Accept';
$string['declineinvitegroup'] = 'Decline';
$string['leftgroup'] = 'You have now left this group';
$string['leftgroupfailed'] = 'Leaving group failed';
$string['couldnotleavegroup'] = 'You cannot leave this group';
$string['joinedgroup'] = 'You are now a group member';
$string['couldnotjoingroup'] = 'You cannot join this group';
$string['grouprequestsent'] = 'Group membership request sent';
$string['couldnotrequestgroup'] = 'Could not send group membership request';
$string['groupjointypeopen'] = 'Membership to this community is open';
$string['groupjointypecontrolled'] = 'Membership to this community  is controlled.  You cannot join this community';
$string['groupjointypeinvite'] = 'Membership to this community is invite only';
$string['groupjointyperequest'] = 'Membership to this community is by request only';
$string['grouprequestsubject'] = 'New community membership request';
$string['grouprequestmessage'] = '%s has sent a membership request to join the community %s';
$string['grouprequestmessagereason'] = '%s has sent a membership request to join the community %s with the reason %s.';
$string['groupconfirmdelete'] = 'Are you sure you wish to delete this community?';
$string['groupconfirmdeletehasviews'] = 'Are you sure you wish to delete this community? Some of your views use this community for access control, removing this community would mean that the members of that community would not have access to the views.';
$string['deletegroup'] = 'Group Deleted Successfully';

$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['all'] = 'All';

// friendslist
$string['reasonoptional'] = 'Reason (optional)';
$string['request'] = 'Request';
$string['updatedfriendcontrolsetting'] = 'Updated friend control setting successfully';

$string['friendformaddsuccess'] = 'Added %s to your friends list';
$string['friendformremovesuccess'] = 'Removed %s from your friends list';
$string['friendformrequestsuccess'] = 'Sent a friendship request to %s';
$string['friendformacceptsuccess'] = 'Accepted friend request';
$string['friendformrejectsuccess'] = 'Rejected friend request';

$string['addtofriendslist'] = 'Add to friends';
$string['requestfriendship'] = 'Request friendship';

$string['addedtofriendslistsubject'] = 'New friend';
$string['addedtofriendslistmessage'] = '%s added you as a friend! This means that %s is also on your friend list now too. '
    . ' Click on the link below to see their profile page';

$string['requestedfriendlistsubject'] = 'New friend request';
$string['requestedfriendlistmessage'] = '%s has requested that you add them as a friend.  '
    .' You can either do this from the link below, or from your friends list page';

$string['requestedfriendlistmessagereason'] = '%s has requested that you add them as a friend.'
    . ' You can either do this from the link below, or from your friends list page.'
    . ' Their reason was:
    ';

$string['removefromfriendslist'] = 'Remove from friends';
$string['confirmremovefriend'] = 'Are you sure you want to remove this user from your friends list?';
$string['removedfromfriendslistsubject'] = 'Removed from friends list';
$string['removedfromfriendslistmessage'] = '%s has removed you from their friends list.';
$string['removedfromfriendslistmessagereason'] = '%s has removed you from their friends list.  Their reason was: ';

$string['friendshipalreadyrequested'] = 'You have requested to be added to %s\'s friends list';
$string['friendshipalreadyrequestedowner'] = '%s has requested to be added to your friends list';
$string['rejectfriendshipreason'] = 'Reason for rejecting request';

$string['friendrequestacceptedsubject'] = 'Friend request accepted';
$string['friendrequestacceptedmessage'] = '%s has accepted your friend request and they have been added to your friends list'; 
$string['friendrequestrejectedsubject'] = 'Friend request rejected';
$string['friendrequestrejectedmessage'] = '%s has rejected your friend request.';
$string['friendrequestrejectedmessagereason'] = '%s has rejected your friend request.  Their reason was: ';

$string['allfriends']     = 'All Friends';
$string['currentfriends'] = 'Current Friends';
$string['pendingfriends'] = 'Pending friends';
$string['backtofriendslist'] = 'Back to Friends List';
$string['findnewfriends'] = 'Find New Friends';
$string['seeallviews']    = 'See all %s views...';
$string['noviewstosee']   = 'None that you can see :(';
$string['whymakemeyourfriend'] = 'This is why you should make me your friend:';
$string['approverequest'] = 'Approve Request!';
$string['denyrequest']    = 'Deny Request';
$string['pending']        = 'pending';
$string['trysearchingforfriends'] = 'Try %ssearching for your friends%s to grow your network!';
$string['nobodyawaitsfriendapproval'] = 'Nobody is awaiting your approval to become your friend';
$string['sendfriendrequest'] = 'Send Friend Request!';
$string['addtomyfriends'] = 'Add to My Friends!';
$string['friendshiprequested'] = 'Friendship requested!';
$string['existingfriend'] = 'existing friend';
$string['nosearchresultsfound'] = 'No search results found :(';

$string['friendlistfailure'] = 'Failed to modify your friends list';
$string['userdoesntwantfriends'] = 'This user doesn\'t want any new friends';
$string['cannotrequestfriendshipwithself'] = 'You cannot request a friendship with yourself';

//messaging between users
$string['messagebody'] = 'Send message';
$string['sendmessage'] = 'Send message';
$string['messagesent'] = 'Message sent!';
$string['messagenotsent'] = 'Failed to send message';
$string['newusermessage'] = 'New message from %s';


$string['friend'] = 'Friend';
$string['profileicon'] = 'Profile Icon';

// general views stuff
$string['allviews'] = 'All views';

// Upload manager
$string['quarantinedirname'] = 'quarantine';
$string['clammovedfile'] = 'The file has been moved to a quarantine directory.';
$string['clamdeletedfile'] = 'The file has been deleted';
$string['clamdeletedfilefailed'] = 'The file could not be deleted';
$string['clambroken'] = 'Your administrator has enabled virus checking for file uploads but has misconfigured something.  Your file upload was NOT successful. Your administrator has been emailed to notify them so they can fix it.  Maybe try uploading this file later.';
$string['clamemailsubject'] = '%s :: Clam AV notification';
$string['clamlost'] = 'Clam AV is configured to run on file upload, but the path supplied to Clam AV, %s, is invalid.';
$string['clamfailed'] = 'Clam AV has failed to run.  The return error message was %s. Here is the output from Clam:';
$string['clamunknownerror'] = 'There was an unknown error with clam.';
$string['image'] = 'Image';
$string['filenotimage'] = 'The file you uploaded is not valid image. It must be a PNG, JPEG or GIF file.';
$string['uploadedfiletoobig'] = 'The file was too big. Please ask your administrator for more information.';
$string['notphpuploadedfile'] = 'The file was lost in the upload process. This should not happen, please contact your administrator for more information.';
$string['virusfounduser'] = 'The file you have uploaded, %s, has been scanned by a virus checker and found to be infected! Your file upload was NOT successful.';
$string['fileunknowntype'] = 'The type of your uploaded file could not be determined. Your file may be corrupted, or it could be a configuration problem. Please contact your administrator.';
$string['filetypenotallowed'] = 'You are not allowed to upload files of this type. Please contact your administrator for more information.';
$string['virusrepeatsubject'] = 'Warning: %s is a repeat virus uploader.';
$string['virusrepeatmessage'] = 'The user %s has uploaded multiple files which have been scanned by a virus checker and found to be infected.';

$string['youraccounthasbeensuspended'] = 'Your account has been suspended';
$string['youraccounthasbeensuspendedtext'] = 'Your account has been suspended'; // @todo: more info?
$string['youraccounthasbeenunsuspended'] = 'Your account has been unsuspended';
$string['youraccounthasbeenunsuspendedtext'] = 'Your account has been unsuspended'; // @todo: more info?

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = 'Afghanistan';
$string['country.ax'] = 'Åland Islands';
$string['country.al'] = 'Albania';
$string['country.dz'] = 'Algeria';
$string['country.as'] = 'American Samoa';
$string['country.ad'] = 'Andorra';
$string['country.ao'] = 'Angola';
$string['country.ai'] = 'Anguilla';
$string['country.aq'] = 'Antarctica';
$string['country.ag'] = 'Antigua and Barbuda';
$string['country.ar'] = 'Argentina';
$string['country.am'] = 'Armenia';
$string['country.aw'] = 'Aruba';
$string['country.au'] = 'Australia';
$string['country.at'] = 'Austria';
$string['country.az'] = 'Azerbaijan';
$string['country.bs'] = 'Bahamas';
$string['country.bh'] = 'Bahrain';
$string['country.bd'] = 'Bangladesh';
$string['country.bb'] = 'Barbados';
$string['country.by'] = 'Belarus';
$string['country.be'] = 'Belgium';
$string['country.bz'] = 'Belize';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermuda';
$string['country.bt'] = 'Bhutan';
$string['country.bo'] = 'Bolivia';
$string['country.ba'] = 'Bosnia and Herzegovina';
$string['country.bw'] = 'Botswana';
$string['country.bv'] = 'Bouvet Island';
$string['country.br'] = 'Brazil';
$string['country.io'] = 'British Indian Ocean Territory';
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bg'] = 'Bulgaria';
$string['country.bf'] = 'Burkina Faso';
$string['country.bi'] = 'Burundi';
$string['country.kh'] = 'Cambodia';
$string['country.cm'] = 'Cameroon';
$string['country.ca'] = 'Canada';
$string['country.cv'] = 'Cape Verde';
$string['country.ky'] = 'Cayman Islands';
$string['country.cf'] = 'Central African Republic';
$string['country.td'] = 'Chad';
$string['country.cl'] = 'Chile';
$string['country.cn'] = 'China';
$string['country.cx'] = 'Christmas Island';
$string['country.cc'] = 'Cocos (Keeling) Islands';
$string['country.co'] = 'Colombia';
$string['country.km'] = 'Comoros';
$string['country.cg'] = 'Congo';
$string['country.cd'] = 'Congo, The Democratic Republic of The';
$string['country.ck'] = 'Cook Islands';
$string['country.cr'] = 'Costa Rica';
$string['country.ci'] = 'Cote D\'ivoire';
$string['country.hr'] = 'Croatia';
$string['country.cu'] = 'Cuba';
$string['country.cy'] = 'Cyprus';
$string['country.cz'] = 'Czech Republic';
$string['country.dk'] = 'Denmark';
$string['country.dj'] = 'Djibouti';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominican Republic';
$string['country.ec'] = 'Ecuador';
$string['country.eg'] = 'Egypt';
$string['country.sv'] = 'El Salvador';
$string['country.gq'] = 'Equatorial Guinea';
$string['country.er'] = 'Eritrea';
$string['country.ee'] = 'Estonia';
$string['country.et'] = 'Ethiopia';
$string['country.fk'] = 'Falkland Islands (Malvinas)';
$string['country.fo'] = 'Faroe Islands';
$string['country.fj'] = 'Fiji';
$string['country.fi'] = 'Finland';
$string['country.fr'] = 'France';
$string['country.gf'] = 'French Guiana';
$string['country.pf'] = 'French Polynesia';
$string['country.tf'] = 'French Southern Territories';
$string['country.ga'] = 'Gabon';
$string['country.gm'] = 'Gambia';
$string['country.ge'] = 'Georgia';
$string['country.de'] = 'Germany';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gr'] = 'Greece';
$string['country.gl'] = 'Greenland';
$string['country.gd'] = 'Grenada';
$string['country.gp'] = 'Guadeloupe';
$string['country.gu'] = 'Guam';
$string['country.gt'] = 'Guatemala';
$string['country.gg'] = 'Guernsey';
$string['country.gn'] = 'Guinea';
$string['country.gw'] = 'Guinea-bissau';
$string['country.gy'] = 'Guyana';
$string['country.ht'] = 'Haiti';
$string['country.hm'] = 'Heard Island and Mcdonald Islands';
$string['country.va'] = 'Holy See (Vatican City State)';
$string['country.hn'] = 'Honduras';
$string['country.hk'] = 'Hong Kong';
$string['country.hu'] = 'Hungary';
$string['country.is'] = 'Iceland';
$string['country.in'] = 'India';
$string['country.id'] = 'Indonesia';
$string['country.ir'] = 'Iran, Islamic Republic of';
$string['country.iq'] = 'Iraq';
$string['country.ie'] = 'Ireland';
$string['country.im'] = 'Isle of Man';
$string['country.il'] = 'Israel';
$string['country.it'] = 'Italy';
$string['country.jm'] = 'Jamaica';
$string['country.jp'] = 'Japan';
$string['country.je'] = 'Jersey';
$string['country.jo'] = 'Jordan';
$string['country.kz'] = 'Kazakhstan';
$string['country.ke'] = 'Kenya';
$string['country.ki'] = 'Kiribati';
$string['country.kp'] = 'Korea, Democratic People\'s Republic of';
$string['country.kr'] = 'Korea, Republic of';
$string['country.kw'] = 'Kuwait';
$string['country.kg'] = 'Kyrgyzstan';
$string['country.la'] = 'Lao People\'s Democratic Republic';
$string['country.lv'] = 'Latvia';
$string['country.lb'] = 'Lebanon';
$string['country.ls'] = 'Lesotho';
$string['country.lr'] = 'Liberia';
$string['country.ly'] = 'Libyan Arab Jamahiriya';
$string['country.li'] = 'Liechtenstein';
$string['country.lt'] = 'Lithuania';
$string['country.lu'] = 'Luxembourg';
$string['country.mo'] = 'Macao';
$string['country.mk'] = 'Macedonia, The Former Yugoslav Republic of';
$string['country.mg'] = 'Madagascar';
$string['country.mw'] = 'Malawi';
$string['country.my'] = 'Malaysia';
$string['country.mv'] = 'Maldives';
$string['country.ml'] = 'Mali';
$string['country.mt'] = 'Malta';
$string['country.mh'] = 'Marshall Islands';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauritania';
$string['country.mu'] = 'Mauritius';
$string['country.yt'] = 'Mayotte';
$string['country.mx'] = 'Mexico';
$string['country.fm'] = 'Micronesia, Federated States of';
$string['country.md'] = 'Moldova, Republic of';
$string['country.mc'] = 'Monaco';
$string['country.mn'] = 'Mongolia';
$string['country.ms'] = 'Montserrat';
$string['country.ma'] = 'Morocco';
$string['country.mz'] = 'Mozambique';
$string['country.mm'] = 'Myanmar';
$string['country.na'] = 'Namibia';
$string['country.nr'] = 'Nauru';
$string['country.np'] = 'Nepal';
$string['country.nl'] = 'Netherlands';
$string['country.an'] = 'Netherlands Antilles';
$string['country.nc'] = 'New Caledonia';
$string['country.nz'] = 'New Zealand';
$string['country.ni'] = 'Nicaragua';
$string['country.ne'] = 'Niger';
$string['country.ng'] = 'Nigeria';
$string['country.nu'] = 'Niue';
$string['country.nf'] = 'Norfolk Island';
$string['country.mp'] = 'Northern Mariana Islands';
$string['country.no'] = 'Norway';
$string['country.om'] = 'Oman';
$string['country.pk'] = 'Pakistan';
$string['country.pw'] = 'Palau';
$string['country.ps'] = 'Palestinian Territory, Occupied';
$string['country.pa'] = 'Panama';
$string['country.pg'] = 'Papua New Guinea';
$string['country.py'] = 'Paraguay';
$string['country.pe'] = 'Peru';
$string['country.ph'] = 'Philippines';
$string['country.pn'] = 'Pitcairn';
$string['country.pl'] = 'Poland';
$string['country.pt'] = 'Portugal';
$string['country.pr'] = 'Puerto Rico';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'Romania';
$string['country.ru'] = 'Russian Federation';
$string['country.rw'] = 'Rwanda';
$string['country.sh'] = 'Saint Helena';
$string['country.kn'] = 'Saint Kitts and Nevis';
$string['country.lc'] = 'Saint Lucia';
$string['country.pm'] = 'Saint Pierre and Miquelon';
$string['country.vc'] = 'Saint Vincent and The Grenadines';
$string['country.ws'] = 'Samoa';
$string['country.sm'] = 'San Marino';
$string['country.st'] = 'Sao Tome and Principe';
$string['country.sa'] = 'Saudi Arabia';
$string['country.sn'] = 'Senegal';
$string['country.cs'] = 'Serbia and Montenegro';
$string['country.sc'] = 'Seychelles';
$string['country.sl'] = 'Sierra Leone';
$string['country.sg'] = 'Singapore';
$string['country.sk'] = 'Slovakia';
$string['country.si'] = 'Slovenia';
$string['country.sb'] = 'Solomon Islands';
$string['country.so'] = 'Somalia';
$string['country.za'] = 'South Africa';
$string['country.gs'] = 'South Georgia and The South Sandwich Islands';
$string['country.es'] = 'Spain';
$string['country.lk'] = 'Sri Lanka';
$string['country.sd'] = 'Sudan';
$string['country.sr'] = 'Suriname';
$string['country.sj'] = 'Svalbard and Jan Mayen';
$string['country.sz'] = 'Swaziland';
$string['country.se'] = 'Sweden';
$string['country.ch'] = 'Switzerland';
$string['country.sy'] = 'Syrian Arab Republic';
$string['country.tw'] = 'Taiwan, Province of China';
$string['country.tj'] = 'Tajikistan';
$string['country.tz'] = 'Tanzania, United Republic of';
$string['country.th'] = 'Thailand';
$string['country.tl'] = 'Timor-leste';
$string['country.tg'] = 'Togo';
$string['country.tk'] = 'Tokelau';
$string['country.to'] = 'Tonga';
$string['country.tt'] = 'Trinidad and Tobago';
$string['country.tn'] = 'Tunisia';
$string['country.tr'] = 'Turkey';
$string['country.tm'] = 'Turkmenistan';
$string['country.tc'] = 'Turks and Caicos Islands';
$string['country.tv'] = 'Tuvalu';
$string['country.ug'] = 'Uganda';
$string['country.ua'] = 'Ukraine';
$string['country.ae'] = 'United Arab Emirates';
$string['country.gb'] = 'United Kingdom';
$string['country.us'] = 'United States';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Uzbekistan';
$string['country.vu'] = 'Vanuatu';
$string['country.ve'] = 'Venezuela';
$string['country.vn'] = 'Viet Nam';
$string['country.vg'] = 'Virgin Islands, British';
$string['country.vi'] = 'Virgin Islands, U.S.';
$string['country.wf'] = 'Wallis and Futuna';
$string['country.eh'] = 'Western Sahara';
$string['country.ye'] = 'Yemen';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'System';
$string['done'] = 'Done';
$string['back'] = 'Back';

?>

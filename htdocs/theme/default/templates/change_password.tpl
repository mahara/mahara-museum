{include file="header.tpl"}

<div id="column-full">
	<div class="content">
		<div class="box-cnrs"><span class="cnr-tl"><span class="cnr-tr"><span class="cnr-bl"><span class="cnr-br">
			<div class="maincontent">
			<h2>{str tag="changepassword"}</h2>
			
			<p>{str tag="changepasswordinfo"}</p>
			
			{$change_password_form}
			</div>
		</span></span></span></span></div>	
	</div>
</div>

{include file="footer.tpl"}

{include file="header.tpl"}

{include file="columnfullstart.tpl"}
{if $can_edit}
<div class="fr editview">
    <span class="settingsicon">
        <a href="blocks.php?id={$viewid}">{str tag=editthisview section=view}</a>
    </span>
</div>
{/if}
<div id="view">
	<h3>
        {foreach name=viewnav from=$VIEWNAV item=item}
          {$item}
          {if !$smarty.foreach.viewnav.last}
            :
          {/if}
        {/foreach}
        </h3>

        {if $DESCRIPTION}
        <p class="view-description">{$DESCRIPTION}</p>
        {/if}
	
            <div id="bottom-pane">
                <div id="column-container">
                    {if $VIEWCONTENT}
                       {$VIEWCONTENT}
                    {/if}
                    <div class="cb">
                    </div>
                </div>
            </div>
	<div id="publicfeedback">
	<table id="feedbacktable">
		<thead>
			<tr><th colspan=5>{str tag=feedback}</th></tr>
		</thead>
	</table>
	</div>
	<div id="viewmenu"></div>
</div>
{include file="columnfullend.tpl"}

{include file="footer.tpl"}

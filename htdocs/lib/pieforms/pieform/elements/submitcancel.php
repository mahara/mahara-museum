<?php
/**
 * This program is part of Pieforms
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * @package    pieform
 * @subpackage element
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

/**
 * Renders a submit and cancel button
 *
 * @param Pieform  $form    The form to render the element for
 * @param array    $element The element to render
 * @return string           The HTML for the element
 */
function pieform_element_submitcancel(Pieform $form, $element) {
    if (!isset($element['value']) || !is_array($element['value']) || count($element['value']) != 2) {
        throw new PieformException('The submitcancel element "' . $element['name']
            . '" must have a two element array for its value');
    }
    $form->include_plugin('element', 'submit');
    $form->include_plugin('element', 'cancel');
    $submitelement = $element;
    $submitelement['value'] = $element['value'][0];
    $cancelelement = $element;
    $cancelelement['value'] = $element['value'][1];
    return pieform_element_submit($form, $submitelement) . ' ' . pieform_element_cancel($form, $cancelelement);
}

function pieform_element_submitcancel_set_attributes($element) {
    $element['submitelement'] = true;
    return $element;
}

function pieform_element_submitcancel_get_value(Pieform $form, $element) {
    if (is_array($element['value'])) {
        return $element['value'][0];
    }
    else {
        return $element['value'];
    }
}

?>

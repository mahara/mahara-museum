<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2007 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Penny Leach <penny@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('PUBLIC', 1);
require('init.php');
require_once('file.php');

$type = param_alpha('type');

switch ($type) {
    case 'profileiconbyid':
    case 'profileicon':
        $id = param_integer('id');
        $size = get_imagesize_parameters();

        if ($type == 'profileicon') {
            // Convert ID of user to the ID of a profileicon
            $id = get_field('usr', 'profileicon', 'id', $id);
        }

        if ($id) {
            if ($path = get_dataroot_image_path('artefact/internal/profileicons', $id, $size)) {
                $type = get_mime_type($path);
                if ($type) {
                    header('Content-type: ' . $type);
                    readfile($path);
                    exit;
                }
            }
        }

        header('Content-type: ' . 'image/png');
        if ($path = theme_get_path('images/no_userphoto' . $size . '.png')) {
            readfile($path);
            exit;
        }
        readfile(theme_get_path('images/no_userphoto40x40.png'));
        break;

    case 'blocktype':
        $bt = param_alpha('bt'); // blocktype
        $ap = param_alpha('ap', null); // artefact plugin (optional)
        
        $basepath = 'blocktype/' . $bt;
        if (!empty($ap)) {
            $basepath = 'artefact/' . $ap . '/' . $basepath;
        }
        header('Content-type: image/png');
        $path = get_config('docroot') . $basepath . '/thumb.png';
        if (is_readable($path)) {
            readfile($path);
            exit;
        }
        readfile(theme_get_path('images/no_thumbnail.png'));
        break;
    case 'viewlayout':
        header('Content-type: image/png');
        $vl = param_integer('vl');
        if ($widths = get_field('view_layout', 'widths', 'id', $vl)) {
            if ($path = theme_get_path('images/vl-' . str_replace(',', '-', $widths) . '.png')) {
                readfile($path);
                exit;
            }
        }
        readfile(theme_get_path('images/no_thumbnail.png'));
        break;
}

?>

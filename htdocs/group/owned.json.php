<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2007 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('JSON', 1);

require(dirname(dirname(__FILE__)) . '/init.php');

json_headers();

$action = param_variable('action');

if ($action == 'delete') {
    $id = param_integer('id');
    // check owner
    $owner = get_field('group', 'owner', 'id', $id);
    if ($owner != $USER->get('id')) {
        json_reply('local', get_string('cantdeletegroupdontown'));
    }
    db_begin();
    delete_records('view_access_group', 'group', $id);
    delete_records('group_member_invite', 'group', $id);
    delete_records('group_member_request', 'group', $id);
    delete_records('group_member', 'group', $id);
    delete_records('group', 'id', $id);
    db_commit();

    json_reply(null, get_string('deletegroup'));
}

json_reply('local', 'Unknown action');

?>
